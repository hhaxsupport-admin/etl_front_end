import logo from './logo.svg';
import './App.css';
import React, {useEffect, useState} from 'react';
import {BrowserRouter, Route, Switch, NavLink} from 'react-router-dom';
import {PayerDashboard} from './PayerDashboard';
import ProviderDashboard from './ProviderDashboard';

function App() {
  
  return (
    <BrowserRouter>
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <a href="#" class="brand-link">
            <span class="brand-text font-weight-light">Job Dashboardtest</span>
        </a>
        <div class="sidebar">
            <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview menu-open">
                  <a href="#" class="nav-link active">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                      Dashboard
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                 
                  <NavLink class="nav-link" to="/PayerDashboard">
                    {/* <i class="far fa-circle nav-icon"></i> */}
                    <p>Payer Dashboard</p>
                  </NavLink>
                  
                  
                  <NavLink class="nav-link" to="/ProviderDashboard">
                  {/* <i class="far fa-circle nav-icon"></i> */}
                  <p>Provider Dashboard</p>
                  </NavLink>
                  
                  </ul>
                </li>
              </ul>
            </nav>
        </div>
      </aside>
      
            <Switch>
                    <Route path="/PayerDashboard" component={PayerDashboard}/>
            </Switch>
            <Switch>
                    <Route path="/ProviderDashboard" component={ProviderDashboard}/>
            </Switch>
    </BrowserRouter>
  );
}

export default App;
