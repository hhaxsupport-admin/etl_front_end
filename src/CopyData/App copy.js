import logo from './logo.svg';
import './App.css';
import {Dashboard} from './Dashboard';
import {DashboardMain} from './DashboardMain';
import React, {useEffect, useState} from 'react';
import {BrowserRouter, Route, Switch, NavLink} from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div className="App container">
        <h3 className="d-flex justify-content-center m-3">
          React JS Frontend
        </h3>
        <nav className="navbar navbar-expand-sm bg-light navbar-dark">
          <ul className="navbar-nav">
            <li className="nav-item- m-1">
              <NavLink className="btn btn-light btn-outline-primary" to="/dashboard">
                Dashboard
              </NavLink>
              <NavLink className="btn btn-light btn-outline-primary" to="/DashboardMain">
                DashboardMain
              </NavLink>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route path="/dashboard" component={Dashboard}/>
          <Route path="/dashboardMain" component={DashboardMain}/>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
