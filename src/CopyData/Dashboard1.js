// import React,{Component} from 'react';
// import { variables } from './Variables';

// export class Dashboard extends Component{
    
//     constructor(props){
//         super(props);
//         this.state = {
//             job_list:[]
//         }
//     }

//     refreshList()
//     {
//         fetch(variables.API_URL)
//         .then(response=>response.json())
//         .then(data=>{
//             this.setState({job_list:data});
//         });
//     }

//     componentDidMount(){
//         this.refreshList();
//     }

//     render(){
//         const{
//             job_list
//         }=this.state;
//         return(
//             <div>
//                 <table className="table table-stripped">
//                     <thead>
//                         <tr>
//                         <th>job_id</th>
//                         <th>name</th>
//                         <th>start_execution_date </th>
//                         <th>stop_execution_date</th>
//                         <th>last_run_outcome</th>
//                         <th>last_run_duration </th>
//                         </tr>
//                     </thead>
//                     <tbody>
//                         {job_list.map(obj=>
//                         <tr key={obj.job_id}>
//                             {obj.total_jobs}
//                             <td>{obj.job_id}</td>
//                             <td>{obj.name}</td>
//                             <td>{obj.start_execution_date}</td>
//                             <td>{obj.stop_execution_date}</td>
//                             <td>{obj.last_run_outcome}</td>
//                             <td>{obj.last_run_duration}</td>
//                         </tr>
//                             )}
//                     </tbody>
//                 </table>
//             </div>
//         )
//     }
// }