import React,{useState, useEffect} from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.css';
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import filterFactory,{textFilter} from 'react-bootstrap-table2-filter';

function ProviderDashboard(){
    const [userList, setUserList] = useState([]);

    const columns = [
        {dataField:'id', text:'Id'},
        {dataField:'name', text:'Name',sort:true,filter:textFilter(),},
        {dataField:'userName', text:'Username'},
        {dataField:'email', text:'Email'}
    ]
    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        .then(result => setUserList(result))
        .catch(error => console.log(error));
    },[])
    return<div>
            <div class="content-wrapper">
            <h3>All Jobs</h3>
            <BootstrapTable bootstrap4 keyField='id' columns={columns} pagination={paginationFactory()} data={userList}
            filter={filterFactory()}/>
            {/* <table>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>UserName</th>
                    <th>Email</th>
                    
                </tr>
                {
                    userList && userList.length> 0 ?
                    userList.map(usr =>
                <tr>
                    <td>{usr.id}</td>
                    <td>{usr.name}</td>  
                    <td>{usr.username}</td>  
                    <td>{usr.email} </td> 
                           
                </tr>
                
                )
                :'Loading'
                }
                
            </table> */}
        </div>
        </div> 
}
export default ProviderDashboard;