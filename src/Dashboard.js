import React,{Component} from 'react';
import { variables } from './Variables';

export class Dashboard extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            // job_list:[],
            total_jobs: [],
            success_jobs: [],
            failed_jobs: [],
            long_running_jobs: [],
            currently_running_jobs: [],
            all_jobs:[]
        }
    }

    refreshList()
    {
        fetch(variables.API_URL)
        .then(response=>response.json())
        .then(data=>{
            // this.setState({job_list:data}),
            this.setState({all_jobs:data.alljob});
        });
    }

    componentDidMount(){
        this.refreshList();
    }

    render(){
        const{ all_jobs }=this.state;
        return(
            <div>
                <table className='table table-stripped'>
                    <thead>
                        <tr>
                        <th>
                        job_id
                        </th>
                        <th>
                        name
                        </th>
                        <th>
                        start_execution_date
                        </th>
                        <th>
                        stop_execution_date
                        </th>
                        <th>
                        last_run_outcome
                        </th>
                        <th>
                        last_run_duration
                        </th>
                        </tr>
                    </thead>
                    <tbody>
                        {all_jobs.map(dep=>
                        <tr key={dep.job_id}>
                            <td>{dep.job_id}</td>
                            <td>{dep.name}</td>
                            <td>{dep.start_execution_date}</td>
                            <td>{dep.stop_execution_date}</td>
                            <td>{dep.last_run_outcome}</td>
                            <td>{dep.last_run_duration}</td>
                        </tr>
                            )}
                    </tbody>
                </table>
            </div>
        )
    }
}