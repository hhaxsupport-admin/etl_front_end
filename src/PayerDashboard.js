import React,{Component} from 'react';
import { variables } from './Variables';
import BootstrapTable from 'react-bootstrap-table-next';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.css';
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import filterFactory,{textFilter} from 'react-bootstrap-table2-filter';

export class PayerDashboard extends Component{




    constructor(props){
        super(props);
        this.handleShowMore = this.handleShowMore.bind(this)
        this.state = {
            total_jobs:0,
            success_jobs:0,
            failed_jobs:0,
            long_running_jobs:0,
            currently_running_jobs:0,
            cancel_jobs:0,
            all_jobs:[]
            
        }
    }

    handleShowMore() {
        this.setState({
          showItems: 
            this.state.showItems >= this.state.items.length ?
              this.state.showItems : this.state.showItems + 1
        })
      }

    refreshList()
    {
        fetch(variables.API_URL)
        .then(response=>response.json())
        .then(data=>{
            this.setState({total_jobs:data.total_jobs});
            this.setState({success_jobs:data.success_jobs});
            this.setState({failed_jobs:data.failed_jobs});
            this.setState({long_running_jobs:data.long_running_jobs});
            this.setState({currently_running_jobs:data.currently_running_jobs});
            this.setState({cancel_jobs:data.cancel_jobs});
            this.setState({all_jobs:data.alljob});       
        });
    }

    componentDidMount(){
        this.refreshList();
    }

    render(){
        let cellvalue ='';
        function statusFormatter(cell, row, rowIndex, formatExtraData) {
           if(cell=='0'){
               cellvalue = 'Failed.'
            }
            else if(cell=='1'){
                cellvalue = 'Success'
            }
            else{
                cellvalue='Cancel'
            }
            return (
                <h5><span class={ formatExtraData[cell] }>{cellvalue}</span></h5>
            );
          }
          function serverFormatter(cell, row, rowIndex, formatExtraData) {
            
             return (
                 <h5><span class={ formatExtraData[cell] }>{cell}</span></h5>
             );
           }
        const{ total_jobs }=this.state;
        const{ success_jobs }=this.state;
        const{ failed_jobs }=this.state;
        const{ long_running_jobs }=this.state;
        const{ currently_running_jobs }=this.state;
        const{ cancel_jobs }=this.state;
        const{ all_jobs }=this.state;
        const columns = [
            {dataField:'job_id', text:'job_id',sort:true,filter:textFilter()},
            {dataField:'name', text:'name',sort:true,filter:textFilter(),},
            {dataField:'start_execution_date', text:'start_execution_date',sort:true,filter:textFilter()},
            {dataField:'stop_execution_date', text:'stop_execution_date',sort:true,filter:textFilter()},
            {dataField:'last_run_outcome', text:'last_run_outcome',formatter: statusFormatter,
            formatExtraData: {
                0: 'badge badge-danger',
                1: 'badge badge-success',
                3: 'badge badge-warning'
            }
            ,sort:true,filter:textFilter()},
            {dataField:'last_run_duration', text:'last_run_duration',sort:true,filter:textFilter()},
            {dataField:'servername', text:'servername',sort:true,filter:textFilter(),
            formatter: serverFormatter,
            formatExtraData: {
                'TELXDEVD01': 'badge badge-warning',
                'TELXSANDBOX02': 'badge badge-danger'
                
            }},
        ];
        const expandRow = {
            renderer: row => (
                <div>
                <p>{ `Name:- ${row.name}` }</p>
                <div>
                     <table>
                         <thead>
                             <th>step_id</th>
                             <th>last_run_outcome</th>
                         </thead>
                         <tbody>
                         {row.step_list.map(({ step_id, last_run_outcome}) =>  <tr key={step_id}>
                                                                               <td>{step_id}</td>
                                                                               <td>{last_run_outcome}</td>
                                                                               </tr>
                          ) }</tbody>
                     </table>
                {/* <p>Step id:- {row.step_list[0]['step_id']} last_run_outcome:- {row.step_list[0]['last_run_outcome']} </p>*/} 
                </div> 
              </div>
            ),
            
            
          }; 
        return(
            <div class="content-wrapper">
                <div class="content-header">
                    <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Payer Dashboard</h1>
                        </div>
                        <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Payer Dashboard </li>
                        </ol>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-4 col-sm-auto col-md-4">
                                <div class="info-box mb-3" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
                                <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-cog"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Jobs</span>
                                        <span class="info-box-number">
                                        <h5>{total_jobs}</h5>
                                        </span>
                                    </div>
                                </div>   
                            </div>
                            <div class="col-4 col-sm-auto col-md-4">
                                <div class="info-box mb-3">
                                    <span class="info-box-icon bg-success elevation-1"><i class="fas fa-check"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Successful Jobs</span>
                                        <span class="info-box-number">
                                        <h5>{success_jobs}</h5>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 col-sm-auto col-md-4">
                                <div class="info-box mb-3">
                                    <span class="info-box-icon bg-danger elevation-2"><i class="fas fa-minus"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Failed Jobs</span>
                                        <span class="info-box-number">
                                        <h5>{failed_jobs}</h5>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 col-sm-auto col-md-4">
                                <div class="info-box mb-3">
                                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-walking"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Currently Running Jobs</span>
                                        <span class="info-box-number">
                                        <h5>{currently_running_jobs}</h5>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 col-sm-auto col-md-4">
                                <div class="info-box mb-3">
                                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-running"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Long Running Jobs</span>
                                        <span class="info-box-number">
                                        <h5>{long_running_jobs}</h5>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 col-sm-auto col-md-4">
                                <div class="info-box mb-3">
                                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-ban"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Cancelled Jobs</span>
                                        <span class="info-box-number">
                                        <h5>{cancel_jobs}</h5>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6">
                                <div class="card">
                                    <div class="card-header border-transparent">
                                        <h2 class="card-title">Recently Created Jobs </h2>
                                        <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                                            <i class="fas fa-times"></i>
                                        </button>
                                        </div>
                                    </div>
                                    <div class="card-body p-0">
                                        <BootstrapTable 
                                        bootstrap4 
                                        keyField='job_id'
                                        striped
                                        hover
                                        condensed
                                        columns={columns}
                                        noDataIndication="Loading..." 
                                        pagination={paginationFactory()} 
                                        data={all_jobs}
                                        expandRow={ expandRow }
                                        filter={filterFactory()}
                                        />
                                    </div>
                                            {/* <table id="dtBasicExample" 
                                            class="table table-striped table-sm" 
                                            cellspacing="0" width="100%">
                                                <thead>
                                                    <th>job_id</th>
                                                    <th>name</th>
                                                    <th>start_execution_date</th>
                                                    <th>stop_execution_date</th>
                                                    <th>last_run_outcome</th>
                                                    <th>last_run_duration</th>
                                                </thead>
                                                <tbody>
                                                {all_jobs
                                                .sort((a,b)=>{
                                                                    let [aDate, aHour] = a.start_execution_date.split("T");
                                                                    let [bDate, bHour] = b.start_execution_date.split("T");
                                                                    let [aDay, aMonth, aYear] = aDate.split("-");
                                                                    let [bDay, bMonth, bYear] = bDate.split("-");
                                                                    let [aHour1, amin, aSecond] = aHour.split(":");
                                                                    let [bHour1, bmin, bSecond] = bHour.split(":");
                                                                    let newADate = `${aYear}/${aMonth}/${aDay} ${aHour1}/${amin}/${aSecond}`;
                                                                    let newBDate = `${bYear}/${bMonth}/${bDay} ${bHour1}/${bmin}/${bSecond}`;   
                                                                    
                                                                    return newBDate.localeCompare(newADate);
                                                                    })
                                                .slice(0,10)
                                                .map(dep=>
                                                    <tr key={dep.job_id}>
                                                        <td>{dep.job_id}</td>
                                                        <td>{dep.name}</td>
                                                        <td>{dep.start_execution_date}</td>
                                                        <td>{dep.stop_execution_date}</td>
                                                        <td>{dep.last_run_outcome}</td>
                                                        <td>{dep.last_run_duration}</td>
                                                    </tr>
                                                    ) }
                                                </tbody>
                                            </table> */}
                                        
                                  
                                    <div class="card-footer clearfix">
                                        {/* <p>
                                            <a class="btn btn-secondary" data-toggle="collapse" 
                                            href="#multiCollapseExample1" role="button" 
                                            aria-expanded="false" aria-controls="multiCollapseExample1">
                                                Show All Jobs
                                            </a>
                                        </p> */}
                                        <div class="row">
                                            <div class="col" >
                                                <div class="collapse multi-collapse" id="multiCollapseExample1">
                                                    <div class="card-body p-0">
                                                        <div class="table-responsive">
                                                            {/* <table id="dtBasicExample" 
                                                            class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <th>job_id</th>
                                                                    <th>name</th>
                                                                    <th>start_execution_date</th>
                                                                    <th>stop_execution_date</th>
                                                                    <th>last_run_outcome</th>
                                                                    <th>last_run_duration</th>
                                                                </thead>
                                                                <tbody>
                                                                {all_jobs
                                                                .sort((a,b)=>{
                                                                                    let [aDate, aHour] = a.start_execution_date.split("T");
                                                                                    let [bDate, bHour] = b.start_execution_date.split("T");
                                                                                    let [aDay, aMonth, aYear] = aDate.split("-");
                                                                                    let [bDay, bMonth, bYear] = bDate.split("-");
                                                                                    let [aHour1, amin, aSecond] = aHour.split(":");
                                                                                    let [bHour1, bmin, bSecond] = bHour.split(":");
                                                                                    let newADate = `${aYear}/${aMonth}/${aDay} ${aHour1}/${amin}/${aSecond}`;
                                                                                    let newBDate = `${bYear}/${bMonth}/${bDay} ${bHour1}/${bmin}/${bSecond}`;   
                                                                                    
                                                                                    return newBDate.localeCompare(newADate);
                                                                                    })
                                                                .map(dep=>
                                                                    <tr key={dep.job_id}>
                                                                        <td>{dep.job_id}</td>
                                                                        <td>{dep.name}</td>
                                                                        <td>{dep.start_execution_date}</td>
                                                                        <td>{dep.stop_execution_date}</td>
                                                                        <td>{dep.last_run_outcome}</td>
                                                                        <td>{dep.last_run_duration}</td>
                                                                    </tr>
                                                                    ) }
                                                                </tbody>
                                                            </table> */}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}