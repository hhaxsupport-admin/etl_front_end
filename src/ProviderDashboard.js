import React,{useState, useEffect} from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.css';
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import filterFactory,{textFilter} from 'react-bootstrap-table2-filter';

function ProviderDashboard(){
    const [userList, setUserList] = useState([]);
    const [stepList, setStepList] = useState([]);
    const columns = [
        {dataField:'job_id', text:'job_id',sort:true,filter:textFilter()},
        {dataField:'name', text:'Name',sort:true,filter:textFilter(),},
        {dataField:'start_execution_date', text:'start_execution_date',sort:true,filter:textFilter()},
        {dataField:'stop_execution_date', text:'stop_execution_date',sort:true,filter:textFilter()},
        {dataField:'last_run_outcome', text:'last_run_outcome',sort:true,filter:textFilter()},
        {dataField:'last_run_duration', text:'last_run_duration',sort:true,filter:textFilter()}
    ]
    const columns1 = [
        {dataField:'step_id', text:'step_id'},
        {dataField:'last_run_outcome', text:'last_run_outcome'}
    ]
    useEffect(() => {
        fetch('https://localhost:44308/api/Home/Index')
        .then(response => response.json())
        .then(result => setUserList(result.alljob))
        .then(result => setStepList(result.alljob.step_list))
        .catch(error => console.log(error));
    },[])
    return<div>
            <div class="content-wrapper">
            <h3>All Jobs</h3>
            <BootstrapTable bootstrap4 keyField='job_id' 
            columns={columns} 
            pagination={paginationFactory()} 
            data={userList}
            filter={filterFactory()}/>

            <BootstrapTable bootstrap4 keyField='step_id' 
            columns={columns1} 
            pagination={paginationFactory()} 
            data={stepList}
            filter={filterFactory()}/>
            
        </div>
    </div> 
}
export default ProviderDashboard;